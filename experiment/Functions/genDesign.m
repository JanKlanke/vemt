function design = genDesign(vpno, seno, blor, subjectCode)
%
% Notes on the design structure:
%
% General info:
%   .nTrain - number of training trials
%   .nTrial - number of test trial
%
% Trial info (.trial is replaced by .train in practice trials):
%   .trial(t).
%
% 2020 by Jan Klanke

global visual scr keys const %#ok<NUSED>

% randomize random
rand('state',sum(100*clock));

% function for rounding away from zero
ceilfix = @(x)ceil(abs(x)) .* sign(x);

% this subject's main sequence parameters
V0 = 450;A0 = 7.9;durPerDeg = 2.7;durInt = 23;

% participant parameters
design.vpno = vpno;
design.seno = str2num(seno);

% condition parameter
design.nTrialsPerCellInBlock = 50; % number of trials per cell in block
design.condRat = blor;             % 1= clock & no fix. period, 2= clock & fix period, 3= loading bar and no fix. period 4= loading bar and fix. period, 5= reaction time,
design.fdbPres = [0 1 0];          % 0 = no feedback, 1 = feedback
design.fdbIndx = [1 1 3];          % 1 = before feedback, 2 = feedback, 3 = after feedback  
design.cloPres = [1 1 0 0 0];      % 0 = no clock, 1 = clock
design.cirPres = [0 0 1 1 0];      % 0 = no loading bar, 1 = loading bar
design.fixPerd = [0 1 0 1 0];      % 0 = no fix. period, 1 = fix. period    
design.reaTime = [0 0 0 0 1];      % 0 = no reation time, 1 = reation time      
      

% create block order
design.blockOrder = vec2mat([1:length(design.fdbPres )*length(design.condRat)],...
   length(design.fdbPres), length(design.condRat));

% stimulus parameters
design.numStim = 1;         % number of stimuli

% settings for fixation (before cue)
design.timFixD = 1.000 * design.fixPerd;        % minimum fixation duration before stim onset [s]
design.timFixJ = 0.000;                         % additional fixation duration jitter before cue onset [s]

% timing settings
design.timAfKe = 0.200;                         % recording time after keypress  [s]
design.timMaSa = 1.000;                         % time to make a saccade (most likely)  [s]
design.iti     = 0.000;                         % inter-trial interval [s]
design.timTrex = 0.500;                         % time the trial gets extended if reaction us not in time (rt condition) [s]
design.timFeba = 0.500;                         % time for which feedback is displayed [s]

% onset settings for stimuli
design.timSti1 = 0.000;                         % delay of stimulus onset (after fixation duration end) [s]
design.timStiD = design.timMaSa;                % stimulus stream duration    [s]

% fixation dot parameters 
design.fixSiz = 0.1;                            % size of the fixation dot [dva]
design.gosSiz = 0.3;

% stimulus parameters
design.numStim = 1;                                                             % number of stimuli
design.stiOri  = 0:(2*pi/360):2*pi-(2*pi/360);                                  % vector with possible stimulus orientations (0:359 deg)
design.appMove = fliplr([cos(design.stiOri); sin(design.stiOri)]);              % vector with possible endpoints of the apperture movement

design.appMoveIdx = round([normrnd(90,25,1,1000), normrnd(180,25,1,1000), ...   % vector with idx of apperture movements 
                    normrnd(270,25,1,1000), normrnd(360,25,1,1000)]);           % normally dist. around the cardinal directions: i.e. peaks at 0, 90, 180, and 270 deg 
design.appMoveIdx(design.appMoveIdx>360) = design.appMoveIdx(design.appMoveIdx>360)-360;  % and cupped at 360 deg

% clock parameters
design.clock.nHand  = 2;        % number of clock hands to be used (either 1 or 2)
design.clock.radius = 5;        % clock radius [dva]
design.clock.handL  = 2;        % length of the clock hand [dva]
design.clock.handW  = 0.1;      % width of the clock hand [dva]
design.clock = getClockPars(design.clock.radius, design.clock.nHand, design.clock.handL, design.clock.handW);

% loading circle parameter
design.circ.radius = 0.75;                % radius of the circular progression bar [dva]
design.circ.width  = 0.1;                 % width of the circular progression bar [dva]
design.circ.color  = 0.3 * visual.white;  % color of the circular progression bar 
design.circ = getCircPars(design.circ.radius, design.circ.width, design.circ.color);

ccount = 0;
for cond = design.condRat
    ccount = ccount + 1;
    for b = 1:length(design.fdbPres)
        t = 0;
        for itri = 1:design.nTrialsPerCellInBlock
            t = t + 1;
            fprintf(1,'\n preparing trial %i ...',t);
            trial(t).trialNum = t; %#ok<*AGROW>

            modi = design.appMoveIdx(ceilfix(length(design.appMoveIdx) * rand));        % select stimulus orientation and endpoint of app-shift            
            feeb = design.fdbPres(b);
            
            % fixation positions
            fixx = -13 +(13 + 13) * rand;   % eccentricity of fixation x (relative to screen center) - can be pos or neg
            fixy =  -4 +( 4 +  4) * rand;   % eccentricity of fixation y (relative to screen center) - can be pos or neg

            % saccade info
            % determine v expected saccade velocities based on amplitudes
            % (using equation  and parameters used in Collewijn, 1988)
            trial(t).sacReq = 0;   % 1 = saccade, 0 = cue but no saccade
            trial(t).sacAmp = 0.5;                    % for a more varied saccade amplitude: normrnd(0.5, 0.1,1);                                      
            trial(t).vpeaks = V0*(1-exp(-trial(t).sacAmp/A0));
            trial(t).sacDur = durPerDeg*trial(t).sacAmp + durInt;       % should be around 25 ms according to Martinez-Conde et al. 2004

            % temporal trial settings
            % duration before target onset [frames]
            trial(t).fixNFr = round((design.timFixD(cond) + design.timFixJ * rand) / scr.fd);
            trial(t).fixDur = trial(t).fixNFr * scr.fd;

            % duration after target onset [frames]
            trial(t).stiNFr = round((design.timSti1 + design.timStiD) / scr.fd);
            trial(t).stiDur = trial(t).stiNFr * scr.fd;
            
            % duration of stimulus contrast ram [frames]
            trial(t).ramNFr = round((design.timStiD/5) / scr.fd);
            trial(t).ramDur = trial(t).ramNFr * scr.fd;
            
            % timing parameters of apperture shift [frames]                             
            trial(t).shiNFr = round((trial(t).sacDur/1000) / scr.fd);       % saccade duration is a good approximation of the duration of the apperture shift 
            trial(t).shiDur = trial(t).shiNFr * scr.fd;
            
            % duration after feedback onset [frames]
            trial(t).texNFr = round(design.timTrex / scr.fd);
            trial(t).texDur = trial(t).texNFr * scr.fd;
            
            % duration after feedback onset [frames]
            trial(t).febNFr = round(design.timFeba / scr.fd);
            trial(t).febDur = trial(t).febNFr * scr.fd;

            % calculate total stimulus duration [frames]
            trial(t).totNFr = trial(t).fixNFr + trial(t).stiNFr;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % generate flag streams for stimulus presentation %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % time of the fixation            
            fixBeg = 1;
            fixEnd = ~design.fixPerd(cond) + trial(t).fixNFr;      
            
            % time, the stimulus is present
            stiBeg = trial(t).fixNFr + 1 + round(design.timSti1 / scr.fd);
            stiEnd = stiBeg + trial(t).stiNFr - 1;
            
            % time, the stimulus is at full contrast
            ctrBeg = stiBeg + trial(t).ramNFr;                              
            ctrEnd = stiEnd - trial(t).ramNFr; 
            
            % time, that the apperture might shift
            shiBeg = ctrBeg + round(trial(t).shiNFr / 2); 
            shiEnd = ctrEnd - round(trial(t).shiNFr / 2); 
            
            % time parameter of apperture movement
            appTop = shiBeg + round(rand * (shiEnd - shiBeg));   % frame no at which the shift of the Gaussian cornel reaches its peak (velocity)
            appStr = appTop - round(trial(t).shiNFr / 2);        % frame no at which the simulated microsaccade starts

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %              spatial information                %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % fixation
            trial(t).fixa.vis = zeros(1,trial(t).totNFr);
            trial(t).fixa.vis(fixBeg:stiEnd) = 1;
            trial(t).fixa.loc = visual.scrCenter + round(visual.ppd * [fixx fixy fixx fixy]);
            trial(t).fixa.sin = round(design.fixSiz * visual.ppd);
            trial(t).fixa.sou = round(design.gosSiz * visual.ppd);
            trial(t).fixa.col = visual.black;
            
            % determine stimulus parameters
            % spatial stimulus settings (standard)
            trial(t).stims.locX = trial(t).fixa.loc(1); 
            trial(t).stims.locY = trial(t).fixa.loc(2);

            % determine stimulus parameters
            % spatial stimulus settings (standard)
            trial(t).stims.col  = repmat(visual.black,design.numStim,1);
            trial(t).stims.pars = getGaborPars(design.numStim);
            trial(t).stims.pars.ori = rad2deg(design.stiOri(modi));
            
            % define change in stimulus position across frames
            trial(t).posVec = zeros(design.numStim*2,trial(t).totNFr); 
            
            % for fixation trials, simulate the saccade
            % (will be replaced by actual saccade data in
            % runTrials ... that's the plan at least)
            % org.: trial(t).posVec(1,ctrBeg:ctrEnd) = normcdf(ctrBeg:ctrEnd, trial(t).shiPeakTp, trial(t).sacDur/4);
            % org.: trial(t).posVec(1,ctrEnd+1:stiEnd) = trial(t).posVec(1,ctrEnd);
            trial(t).posVec(:,shiBeg:shiEnd)   = repmat(normcdf(shiBeg:shiEnd, appTop, trial(t).sacDur/4), 2,1);
            trial(t).posVec(:,ctrEnd+1:stiEnd) = trial(t).posVec(1,ctrEnd);
                        
            % scale by actual saccade amplitude and
            % displace from initial fixation position
            % for the moment, this assumes horizontal
            % saccades. Also translates degrees to pixels.
            % org.: trial(t).posVec(1,:) = dva2pix(modi*trial(t).sacAmp*trial(t).posVec(1,:),scr);  
            trial(t).posVec = dva2pix(trial(t).sacAmp .* design.appMove(:,modi) .* trial(t).posVec,scr);

            % define modulation of top velocity across frames
            % (here, we are keeping stimulus velocity constant)
            velVec = ones(1,length(stiBeg:stiEnd));
            trial(t).stims.tmpfrq = 0; 

            % changed from saccade dir. to movement 
            % dir because one can no longer infer the
            % saccade direction from  stimulus properties 
            % (as has been done originally)
            % org.: trial(t).stims.evovel = modi*(repmat(velVec*trial(t).vpeaks,design.numStim,1) + repmat(trial(t).stims.tmpfrq ./ trial(t).stims.pars.frq',1,length(velVec))); % desired stimulus velocity [dva per sec]
            trial(t).stims.evovel = repmat(velVec*trial(t).vpeaks,design.numStim,1) + repmat(trial(t).stims.tmpfrq ./ trial(t).stims.pars.frq',1,length(velVec)); % desired stimulus velocity [dva per sec]

            % define phase change per frame for entire profile
            phaFra = scr.fd*trial(t).stims.evovel.*repmat(trial(t).stims.pars.frq'*360,1,length(velVec));  % phase change per frame [deg per fra]

            % define stimulus visibility and velocity
            trial(t).stims.vis = zeros(design.numStim,trial(t).totNFr);
            trial(t).stims.pha = zeros(design.numStim,trial(t).totNFr);
            trial(t).stims.vis(:,stiBeg:stiEnd) = 1;
            trial(t).stims.pha(:,stiBeg:stiEnd) = cumsum(phaFra,2);

            % add random phase to each stimulus
            trial(t).stims.pha = trial(t).stims.pha + repmat(360*rand(design.numStim,1),1,trial(t).totNFr);

            % define modulation of contrast across time
            ampVec = ones(1,length(stiBeg:stiEnd));
            ramVec = normcdf(1: trial(t).ramNFr, trial(t).ramNFr/2, trial(t).ramNFr/6);
            ampVec(1: trial(t).ramNFr) = ramVec;
            ampVec(end:-1:(end- trial(t).ramNFr+1)) = ramVec;
            trial(t).stims.evoamp = repmat(trial(t).stims.pars.amp',1,length(ampVec)) .* repmat(ampVec,design.numStim,1);
            trial(t).stims.amp = zeros(design.numStim,trial(t).totNFr);
            trial(t).stims.amp(:,stiBeg:stiEnd) = trial(t).stims.evoamp;
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %         response & feedback information         %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % define clock parameter
            if design.cloPres(cond)
                trial(t).clock = design.clock;   
                cRand = ceilfix(trial(t).clock.Speed / trial(t).clock.nHand * rand);
                cRandR = cRand;
                respOpt = trial(t).clock.Speed;
                trial(t).clock.facePos = design.clock.facePos + trial(t).fixa.loc(1:2)';
                trial(t).clock.handPos = design.clock.handPos + trial(t).fixa.loc';
                if rand < 0.5, trial(t).clock.faceCol = fliplr(design.clock.faceCol); end 
                
                % define clock visibility
                trial(t).clock.vis = zeros(1,trial(t).totNFr);
                if design.fixPerd(cond)                      
                    trial(t).clock.vis(fixBeg:fixEnd) = 1; 
                else 
                    trial(t).clock.vis(fixBeg:stiEnd) = 1;
                end
            end 
                
            % define parameters for circular progression bar
            if design.cirPres(cond)
                trial(t).circ = design.circ;               
                cRand = ceilfix(design.circ.Speed * rand);
                cRandR = cRand;
                respOpt = design.circ.reportOpt;
                trial(t).circ.pos = design.circ.pos + trial(t).fixa.loc(1:2)';
                
                % define visibility of circle
                trial(t).circ.vis = zeros(1,trial(t).totNFr);  
                if design.fixPerd(cond)                               
                    trial(t).circ.vis(fixBeg:fixEnd) = 1; 
                else 
                    trial(t).circ.vis(fixBeg:stiEnd) = 1;
                end
            end
            
            % define parameters for reaction time condition
            if design.reaTime(cond)
                cRand = 0;
                cRandR = 0;
                respOpt = 1;
            end
            
            % Block name messages (per the 4 different conditions)
            if design.cloPres(cond)     && ~design.fixPerd(cond), mesCond = 'Clock present DURING stimulus presentation';
            elseif design.cloPres(cond) && design.fixPerd(cond), mesCond = 'Clock present BEFORE stimulus presentation';
            elseif design.cirPres(cond) && ~design.fixPerd(cond), mesCond = 'Progress circle present DURING stimulus presentation';
            elseif design.cirPres(cond) && design.fixPerd(cond), mesCond = 'Progress circle present BEFORE stimulus presentation';
            else mesCond = 'Reaction time';
            end
            
            % Block name messages (feedback vs. no feedback)
            if feeb, mesFeeb = '(with feedback)'; 
            else mesFeeb = '(without feedback)';
            end      
            trial(t).message = strcat([mesCond,' ', mesFeeb]);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % define critical events during stimulus presentation %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            trial(t).events = zeros(1,trial(t).totNFr);
            trial(t).events(fixBeg) = 1;
            trial(t).events(fixEnd) = 2;
            trial(t).events(stiBeg) = 3;
            trial(t).events(ctrBeg) = 4;
            trial(t).events(appStr) = 5;
            trial(t).events(ctrEnd) = 6;
            trial(t).events(stiEnd) = 7;

            % time requirements for responses
            trial(t).maxSac = design.timMaSa;
            trial(t).aftKey = design.timAfKe;

            % store trial features
            trial(t).expcon = cond;                         % 1 = clock during stim, 2 = clock before stim, 3 = reaction time, 4 = color wheel,
            trial(t).feedback = design.fdbIndx(b) + feeb;   % 1 = feedback, 0 = no feedback
            trial(t).clockpre = design.cloPres(cond);       % 1 = clock present, 0 = clock not present
            trial(t).fixaperi = design.fixPerd(cond);       % 1 = with initial fixation period, 0 = without initial fixation period
            trial(t).circpres = design.cirPres(cond);       % 1 = circular bar present, 0 = circular bar not present
            trial(t).reactime = design.reaTime(cond);       % 1 = reaction time matters, 0 = reaction time does not matter
                                  
            % store stimulus features
            trial(t).fixpox = fixx;                         % x coordinate of position of fixation [dva rel. to midpoint]
            trial(t).fixpoy = fixy;                         % y coordinate of position of fixation [dva rel. to midpoint] 
            trial(t).stiori   = rad2deg(design.stiOri(modi));   % stimulus orientation [radian]
            trial(t).stiphad  = sign(sum(trial(t).stims.pha));  % direction of phase shift [leftward (-1), rightward (1)]
            trial(t).appmovdX = design.appMove(1,modi);         % direction of x component of apperture shift [leftward (-1), rightward (1)]
            trial(t).appmovdY = design.appMove(2,modi);         % direction of y component of apperture shift [up (-1), down (1)]
            trial(t).appmovfrm  = appStr - trial(t).fixNFr;     % time point of START of apperture shift [frame]       
            trial(t).appmovtpst = (appStr - trial(t).fixNFr) * (scr.fd*1000); % time point of START of apperture shift [ms]
            trial(t).appmovtphv = (appTop - trial(t).fixNFr) * (scr.fd*1000); % time point of HIGHEST VELOCITY of the stimulus [ms]
            trial(t).appmovdura = trial(t).shiNFr * (scr.fd*1000);            % duration of stimulus shift [ms]
            trial(t).appampl = trial(t).sacAmp;             % amplitude of stimulus' position change
            trial(t).appvelo = trial(t).vpeaks;             % peak velocity of stimulus position change

            % store some clock features
            trial(t).cSpeed = ~design.reaTime(cond) * scr.refr * 2;                                   % clock speed as points on track
            trial(t).cSpeedpDgr = ~design.reaTime(cond) * (trial(t).cSpeed / 360 * (scr.fd*1000));    % clock speed as duration per degree [ms]
            trial(t).respOpt = ~design.reaTime(cond) * respOpt;                                       % number of potential responses 
            trial(t).onPos = ~design.reaTime(cond) * cRand;                                           % onset position of clock hand [frame idx]
            trial(t).onPosR = ~design.reaTime(cond) * cRandR;                                         % onset position of clock hand [frame idx]
            trial(t).ofPos = ~design.reaTime(cond) * trial(t).onPos + trial(t).stiNFr ;               % offset position of clock hand [frame idx]
            trial(t).fbPos =  ~design.reaTime(cond) * cRand + appStr - trial(t).fixNFr;               % position of clockhand at thev onset of the apperture shift [frame idx]
            trial(t).onPosDgr = ~design.reaTime(cond) * (cRand / trial(t).cSpeed) * 360;              % onset position of clock hand [degree]
            trial(t).fbPosDgr = ~design.reaTime(cond) * (trial(t).fbPos / trial(t).cSpeed) * 360;     % position of clockhand at the onset of the apperture shift [degree]
        end
        r = randperm(t);    % randomise the trials per block
        design.b(design.blockOrder(ccount,b)).trial = trial(r);  % randomise in which order the condtions are presented
    end
end

design.nBlocks = length(design.condRat) * b;                             % number of blocks
design.blockOrder = reshape(design.blockOrder(randperm(length(design.condRat),length(design.condRat)),:)',1,[]);   % block order
design.b = design.b(design.blockOrder); 



design.nTrialsPB = t;                           % number of trials per Block

save(sprintf('%s.mat',subjectCode),'design','visual','scr','keys','const');
