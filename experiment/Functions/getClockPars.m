function pars = getClockPars(clockRad, handn, handL, handW)
% 2014 by Martin Rolfs,
% 2017 by Luke Pendergrass 
% 2018 and 2019 by Jan Klanke

global visual scr %#ok<NUSED>

% make sure that input in output
pars.clockRad = clockRad;
pars.nHand = handn;
pars.handL = handL; 
pars.handW = handW;

% Appearance Parameters
pars.Cradius = round(clockRad * visual.ppd);     % clock radius [dva]
pars.handLength = round(handL * visual.ppd);     % length of clockhand [dva]
pars.handWidth  = round(handW * visual.ppd);     % width of clockhand [dva]
nDots = 320;                                     % number of dots that make up the clock face potentals 320, 480 
numBands = 16;                                   % number of bands that make up the clock face
bandCol1 = .7 * visual.white;                    % color, expressed as monitor luminance
bandCol2 = .3 * visual.white;                    % color, expressed as monitor luminance      
pars.handCol = visual.black;                  

% Speed parameters
pars.Speed = scr.refr * 2;                  % clock speed as points on track, scr.refr * 2 := clockhand traverses half the clock face per second  
pars.speedpDeg = 360 / (pars.Speed * (scr.fd * 1000));  % clock speed time per degree [ms] 

% Report Options
pars.reportOpt = pars.Speed;

% clock spatial parameter calculations
anglesDegr = linspace(0, 360 - 360 / pars.Speed, pars.Speed);
anglesRadr = anglesDegr * (pi / 180);
VectorXunshifted = cos(anglesRadr) .* pars.Cradius;
VectorYunshifted = sin(anglesRadr) .* pars.Cradius;
VectorX = circshift(VectorXunshifted, [(round(length(VectorXunshifted) / 4)),...
    (round(length(VectorYunshifted) / 4))]);
VectorY = circshift(VectorYunshifted, [(round(length(VectorYunshifted) / 4)),...
    (round(length(VectorYunshifted) / 4))]);

VectorRXunshifted = cos(anglesRadr) .* (pars.handLength + pars.Cradius);
VectorRYunshifted = sin(anglesRadr) .* (pars.handLength + pars.Cradius);
VectorRY = circshift(VectorRYunshifted, [(round(length(VectorRYunshifted) / 4)),...
    (round(length(VectorRYunshifted) / 4))]);
VectorRX = circshift(VectorRXunshifted, [(round(length(VectorRXunshifted) / 4)),...
    (round(length(VectorRYunshifted) / 4))]);


% clock face parameter
bandLength = nDots / numBands;
colVec1 = repmat(bandCol1, 1, bandLength);
colVec2 = repmat(bandCol2, 1, bandLength);
pars.faceCol = repmat([colVec1 colVec2], 3, numBands / 2);
pars.faceCol = [pars.faceCol pars.faceCol(:,1)];

% combine x and y coordinates of clock hand and -report hand vectors into
% one
idx = 1:pars.Speed/nDots:pars.Speed;
pars.facePos = [VectorX(idx) VectorX(1); VectorY(idx) VectorY(1)];   
pars.handPos = [VectorX;  VectorY;  VectorRX; VectorRY];

end