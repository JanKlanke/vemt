function [blor] = getConditions
% 2019 by Jan Klanke

% ask for conditions to be displayed
blor = input('>>>> Enter condition numbers:  ','s');
if length(blor) >= 1
    blor = str2num(blor);
elseif isempty(blor)
    blor = [1 2 5];
end

end