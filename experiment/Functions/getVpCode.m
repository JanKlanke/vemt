function [vpno, seno, subjectCode] = getVpCode(experimentName)
%
% asks for subject-ID
% input: exptstr (string) name of experiment
%
% 2010 by Martin Rolfs 
% adapted 2019 by Jan Klanke

FlushEvents('keyDown');

flag = 1; 
% Get participant ID
while flag
    vpno = input('>>>> Enter participant ID:  ','s');
    if isempty(vpno)
        vpno = sprintf('x');
    end
    if length(vpno) == 1
        flag = 0;
    elseif length(vpno) > 1
        fprintf(1, 'WARNING:  Subject code too long. Pls only use 1 letter/number.\n')
    end
end

% Get session number
seno = input('>>>> Enter session number:  ','s');
if length(seno)==1
    seno = strcat('0',seno);
elseif isempty(seno)
    seno = sprintf('01');
end

% Combine participant ID, session number, and experiment name
subjectCode = sprintf(strcat(experimentName,'_',vpno,seno));
end
