function [data,eyeData] = runSingleTrial(td)
%
% td = trial td
%
% 2016 by Martin Rolfs 
% adapted 2019 by Jan Klanke

global scr visual keys const re

% clear keyboard buffer
FlushEvents('KeyDown');
if ~const.TEST == 1, HideCursor(); end

% Set the transparency for gabor patch
% Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);

% function for rounding away from zero
ceilfix = @(x)ceil(abs(x)) .* sign(x);

% predefine boundary information
cxm = td.fixa.loc(1);
cym = td.fixa.loc(2);
rad = visual.boundRad;
chk = visual.fixCkRad;

% draw trial information on operator screen
if ~const.TEST, Eyelink('command','draw_box %d %d %d %d 15', (cxm-chk)*2, (cym-chk)*2, (cxm+chk)*2, (cym+chk)*2); end

% generate Procedural Gabor textures
nStim = length(td.stims.pars.sizp);
sti.tex = visual.procGaborTex;
sti.vis = td.stims.pars.sizp;
sti.src = [zeros(2,nStim); sti.vis; sti.vis].';
sti.dst = CenterRectOnPoint(sti.src, td.stims.locX,td.stims.locY);
for f= 1:td.totNFr, stiFrames(f).dst = sti.dst.' + repmat(td.posVec(:,f),2,nStim); end


% predefine time stamps
tFixaOn = NaN;  % t of fixation on
tFixaOf = NaN;  % t of fixation on
tStimOn = NaN;  % t of stimulus stream on
tStimCf = NaN;  % t of stimulus at full contrast
tStimMv = NaN;  % t of stimulus staring to move
tStimCd = NaN;  % t of stimulus no longer at full contrast
tStimOf = NaN;  % t of stimulus off
tRes    = NaN;  % t of response (if any)
tClr    = NaN;  % t of of clear screen

% set flags before starting stimulus stream
eyePhase  = 1;  % 1 is fixation phase, 2 is saccade phase
breakIt   = 0;
fixBreak  = 0;
saccade   = 0;

% eyeData
eyeData = [];

% Initialize vector important for response
pressed = 0;

% Initialize vector to store data on timing
frameTimes   = NaN(1,td.totNFr);

% flip screen to start out time counter for stimulus frames
firstFlip = 0;nextFlip = 0;
while ~firstFlip, firstFlip = PsychProPixx('QueueImage', scr.myimg); end

f = 0;              % set frame count to 0
tLoopBeg = GetSecs; % get a first timestap
t = tLoopBeg;

while ~breakIt && f < td.totNFr
    f = f+1;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % stimulus presentation %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
    for slo = 1:const.sloMo
        Screen('FillRect', scr.myimg, visual.bgColor);

        % stimuli
        % Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);
        if td.stims.vis(f)
            Screen('DrawTextures', scr.myimg, sti.tex, sti.src, stiFrames(f).dst, td.stims.pars.ori, [], [], [], [], kPsychDontDoRotation, [td.stims.pha(:,f), td.stims.pars.frqp', td.stims.pars.sigp', td.stims.amp(:,f), td.stims.pars.asp', zeros(nStim,3)]');
            drawCirc(visual.bgColor, td.fixa.loc, visual.fixCkRad, scr.myimg);
        end
        % clockface
        if td.clockpre && td.clock.vis(f)  
            Screen('DrawDots', scr.myimg, td.clock.facePos, 3, td.clock.faceCol, [], 1); % draws clockface by means of 3 pixel dots
            drawClockhandL(td.onPos+f-1, td.clock.handPos, td.clock.handWidth, td.clock.handCol, td.clock.nHand, scr.myimg);
        end
        % circular "loading bar"
        if td.circpres && td.circ.vis(f)        
            drawCircBar(td.onPos+f-1, td.onPos, td.circ.pos, td.circ.circW, td.circ.color, scr.myimg);
        end
        % fixation
        if td.fixa.vis(f)
            drawFixation(td.fixa.col, td.fixa.loc, td.fixa.sin, scr.myimg);            
        end
        % Flip
        nextFlip = PsychProPixx('QueueImage', scr.myimg);
    end
    frameTimes(f) = GetSecs;
    
    % get reaction time if this is an rt trial
    if td.reactime && ~pressed
        [pressed, tRes] = checkTarPress(keys.resSpace);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%
    % raise stimulus flags %
    %%%%%%%%%%%%%%%%%%%%%%%%

    % Send message that stimulus is now on
    if isnan(tFixaOn) && td.events(f)==1
        if ~const.TEST  ; Eyelink('message', 'EVENT_FixaOn'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_FixaOn'); end
        tFixaOn = frameTimes(f);
    end
    if isnan(tFixaOf) && td.events(f)==2
        if ~const.TEST  ; Eyelink('message', 'EVENT_FixaOf'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_FixaOf'); end
        tFixaOf = frameTimes(f);
    end
    if isnan(tStimOn) && td.events(f)==3
        if ~const.TEST  ; Eyelink('message', 'EVENT_StimOn'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_StimOn'); end
        tStimOn = frameTimes(f);
    end
    if isnan(tStimCf) && td.events(f)==4
        if ~const.TEST  ; Eyelink('message', 'EVENT_StimCf'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_StimCf'); end
        tStimCf = frameTimes(f);
    end
    if isnan(tStimMv) && td.events(f)==5
        if ~const.TEST  ; Eyelink('message', 'EVENT_StimMv'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_StimMv'); end
        tStimMv = frameTimes(f);
    end
    if isnan(tStimCd) && td.events(f)==6
        if ~const.TEST  ; Eyelink('message', 'EVENT_StimCd'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_StimCd'); end
        tStimCd = frameTimes(f);
    end
    if isnan(tStimOf) && td.events(f)==7
        if ~const.TEST  ; Eyelink('message', 'EVENT_StimOf'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_StimOf'); end
        tStimOf = frameTimes(f);
    end
    
    % eye position check
    if const.TEST<2
        [x,y] = getCoord;
        if sqrt((x-cxm)^2+(y-cym)^2)>chk    % check fixation in a circular area
            fixBreak = 1;
        end
    end
    if fixBreak
        breakIt = 1;    % fixation break
    end
    t = GetSecs;
end

lastFlip = 0;
while ~lastFlip && ~nextFlip
    Screen('FillRect', scr.myimg, visual.bgColor);
    lastFlip = PsychProPixx('QueueImage', scr.myimg);
end
tLoopEnd = GetSecs;

f = 0;
while td.reactime && ~pressed && f < td.texNFr && ~breakIt
    f = f + 1;
    for slo = 1:const.sloMo
        % Continue drawing the background and the fixation dot
        Screen('FillRect', scr.myimg, visual.bgColor);
        drawFixation(td.fixa.col, td.fixa.loc, td.fixa.sin, scr.myimg);
        
        % get response
        [pressed, tRes] = checkTarPress(keys.resSpace);
        nextFlip = PsychProPixx('QueueImage', scr.myimg);
        
        % if no response follows
        if f == td.texNFr && ~pressed
            perfFeedback(sprintf('Too slow.'), td.fixa.loc(1), td.fixa.loc(2));
        end        
        
        % eye position check
        if const.TEST<2
            [x,y] = getCoord;
            if sqrt((x-cxm)^2+(y-cym)^2)>chk    % check fixation in a circular area
                fixBreak = 1;
            end
        end
        if fixBreak
            breakIt = 1;    % fixation break
        end
    end
end

lastFlip = 0;
while ~lastFlip && ~nextFlip
    Screen('FillRect', scr.myimg, visual.bgColor);
    lastFlip = PsychProPixx('QueueImage', scr.myimg);
end

%%%%%%%%%%%%%%%%%%%%%%%%%
% stimulus presentation %
%%%%%%%%%%%%%%%%%%%%%%%%%
% Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
for i = 1:scr.rate
    Screen('FillRect', scr.myimg, visual.bgColor);
    PsychProPixx('QueueImage', scr.myimg);
end

% Init rotary encoder, reset if necessary
ticks = re.getStatus();
if ticks ~= 0, re.resetEncoder(); end
WaitSecs(td.aftKey/4);

switch breakIt
    case 1
        data = 'fixBreak';
        if const.TEST<2, Eyelink('command','draw_text 100 100 15 Fixation break'); end
    otherwise
        % Snd('Play',[repmat(0.5,1,1050) linspace(0.5,0.0,50)].*[zeros(1,1000) sin(1:100)],5000);
        % nec. Parameters
        
        tResBeg = GetSecs;
        while ~td.reactime && ~pressed
            
            % get rotary encoder and response tick(s)
            [ticks, pressed] = re.getStatus(); 
            ticks = -ticks + td.onPosR;         % add onset position of clock hand and flip ticks
            ticks(ticks < 0) = -ceilfix(ticks/ td.respOpt) * td.respOpt + ticks(ticks < 0);  % make sure that ticks are always between 0 and max repsonse options
           
            % create response graphics for clock 
            for i = 1:scr.rate
                Screen('FillRect', scr.myimg, visual.bgColor);         % draw background
                if td.clockpre                                         % draw response onscreen
                    Screen('DrawDots', scr.myimg, td.clock.facePos, 3, td.clock.faceCol, [], 1); % draws clockface by means of 3 pixel dots
                    drawClockhandL(ticks, td.clock.handPos, td.clock.handWidth, td.clock.handCol, td.clock.nHand, scr.myimg); % draw reporthand
                elseif td.circpres
                    drawCircBar(ticks, td.onPos, td.circ.pos, td.circ.circW, td.circ.color, scr.myimg);  % draw loading bar
                end
                PsychProPixx('QueueImage', scr.myimg);
            end
            tRes = GetSecs();
        end
        
        % reset encoder 
        re.resetEncoder();
        
        % evaluate responses
        if td.reactime
            spaKey  = pressed;                         % space key was or was not pressed 
            ticks = NaN;                               % see below for explanations
            ticksCor = NaN;                             
            repDeg  = NaN;                            
            repDegCor = NaN;
            precisionNFr = NaN;                           
            precisionDeg = NaN;
            precisionMs = (tRes - tLoopBeg) * 1000 - td.appmovtpst;
            tResDur   = (tRes - tLoopBeg) * 1000;      % differences in timestamps between the beginn of presentation and pressing of the space bar [ms]
        elseif ~td.reactime
            spaKey  = 0;                                                    
            if td.clockpre && ticks < td.onPos         % account for the fact that there are 2 clock hands (and you don"t know which is the one
                ticksCor = ticks + td.respOpt/2;       % reported on by the participant i.e. add respOption/2 if... 
            elseif td.clockpre && ticks > td.ofPos     % a) response position is before onset position and...
                ticksCor = ticks - td.respOpt/2;       % b) subtract the same if it is after its offset position.
            elseif td.circpres && td.onPos >  td.respOpt/2 && ticks < td.onPos % If in the loading bar condition, the bar crossed over 0 deg,  
                ticksCor = ticks + td.respOpt;         % the respOpt have to be subtracted from the ticks.
            else
                ticksCor = ticks;
            end
            ticksCor = ticksCor - td.onPos;              % finally, recalculate the position of the clockhand relative to trialonset
            repDeg  = (ticks / td.respOpt) * 360;                           % calculate onset position in degree for UNCORRECTED report index [deg]
            repDegCor  = (ticksCor / td.respOpt) * 360;                     % calculate onset position in degree for CORRECTED report index[deg]
            precisionNFr = ticksCor - td.appmovfrm;                         % precision of response [frames]
            precisionDeg = repDegCor - ((td.appmovfrm / td.respOpt) * 360); % precision of response [degree]  
            precisionMs  = (ticksCor - td.appmovfrm) * (scr.fd*1000);       % precision of response [ms]  
            tResDur   = (tRes - tResBeg) * 1000;                            % difference in timestamps between the beginning of the response period and the pressing of the power mate [ms]
        end
        WaitSecs(td.aftKey);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%
        % feedback presentation %
        %%%%%%%%%%%%%%%%%%%%%%%%%
        if ~mod(td.feedback,2)
            for f = 1:td.febNFr
                for slo = 1:const.sloMo
                    Screen('FillRect', scr.myimg, visual.bgColor);           % Paint background
                    
                    % draw clock and clock hand of feedback idx
                    if td.clockpre
                        Screen('DrawDots', scr.myimg, td.clock.facePos, 3, td.clock.faceCol, [], 1); % draws clockface by means of 3 pixel dots
                        drawClockhandL(ticks, td.clock.handPos, td.clock.handWidth, td.clock.handCol, td.clock.nHand, scr.myimg);  % draw clockhand of participant response
                        drawClockhandL(td.fbPos, td.clock.handPos, td.clock.handWidth, visual.white, td.clock.nHand, scr.myimg); % draw clockhand of correct response
                    
                    % draw circular loading bar of 1:feedback idx
                    elseif td.circpres                
                        drawCircBar(td.fbPos, td.onPos, td.circ.pos, td.circ.circW, td.circ.color, scr.myimg);  % draw circular loadind bar
                    
                    % draw text with response time 
                    elseif td.reactime && pressed
                        str = sprintf('%.0f ms',  round(tResDur - td.appmovtpst));
                        drawText(str, td.fixa.loc(1), td.fixa.loc(2));       % draw feedback text     
                    end
                    nextFlip = PsychProPixx('QueueImage', scr.myimg);
                end
            end
        end
        
        lastFlip = 0;
        while ~lastFlip && ~nextFlip
            Screen('FillRect', scr.myimg, visual.bgColor);
            lastFlip = PsychProPixx('QueueImage', scr.myimg);
        end
        
        for f = 1:scr.rate
            Screen('FillRect', scr.myimg, visual.bgColor);
            PsychProPixx('QueueImage', scr.myimg);
        end
        
        tClr = GetSecs;
        if const.TEST<2, Eyelink('message', 'EVENT_Clr'); end
        if const.TEST; fprintf(1,'\nEVENT_Clr'); end 
        
        %-------------------------%
        % PREPARE DATA FOR OUTPUT %
        %-------------------------%
        % collect trial information
        condData = sprintf('%i\t%i\t%i\t%i\t%i\t%i',...
            [td.expcon td.feedback td.clockpre td.fixaperi td.circpres td.reactime]);            % in results, cells  5:10
        
        % collect stimulus information
        stimData = sprintf('%.2f\t%.2f\t%.2f\t%i\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f',...  % in results, cells 11:21
            [td.fixpox td.fixpoy td.stiori td.stiphad td.appmovdX td.appmovdY td.appmovtpst td.appmovtphv td.appmovdura td.appampl td.appvelo]);
        
        % collect clock information
        clokData = sprintf('%i\t%.2f\t%i\t%i\t%.2f\t%i\t%.2f',...                                % in results, cells 22:28
            [td.cSpeed td.cSpeedpDgr td.respOpt td.onPos td.onPosDgr td.fbPos td.fbPosDgr]); 
                           
        % collect time information 
        timeData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i',...                              % in results, cells 29:37
            round(1000*([tFixaOn tFixaOf tStimOn tStimCf tStimMv tStimCd tStimOf tRes tClr]-tStimOn)));
        
        % collect response information 
        respData = sprintf('%i\t%i\t%i\t%.2f\t%.2f\t%i\t%.2f\t%.2f\t%.2f',...                    % in results, cells 38:46
            [spaKey ticks ticksCor repDeg repDegCor precisionNFr precisionDeg precisionMs tResDur]);               

        % get information about how timing of frames worked
        tLoopFrames = round((tLoopEnd-tLoopBeg)/scr.fd);
        frameData = sprintf('%i\t%i',tLoopFrames,td.totNFr);

        % collect data for tab [15 x trialData %i, 5 x timeData %i, 7 x respData %i, 2 x frameData %i]
        data = sprintf('%s\t%s\t%s\t%s\t%s\t%s',condData, stimData, clokData, timeData, respData, frameData);
end
