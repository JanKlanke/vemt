function drawLandMarks(col,mar)
%
% 2008 by Martin Rolfs

global scr visual

pu = round(visual.ppd*0.1);
    
Screen('DrawDots',scr.main,mar,pu,col);

