load('AVesAC99_timeData1.mat');
%%

nts = length(timeData);
nfs = length(timeData(1).realFrameTimes);

nMissed = [timeData(:).numMissedFrames]; 

mfis = [];
for ti=1:nts
    mfis = [mfis timeData(ti).missedFrameIs];
end

figure; hist(mfis,20);

%%

fd = 1/60;

ti=1; 

figure; hold on; 
plot(1:nfs,timeData(ti).lumSeq(1).orig,'b.-'); 
plot(timeData(ti).realFrameTimes/fd,timeData(ti).lumSeq(1).orig,'k.-'); 
plot(1:length(timeData(ti).lumSeq(1).actual),timeData(ti).lumSeq(1).actual,'r.-'); 
xlim([0 30])