function design = genDesignPretest(vpcode)
%
% Notes on the design structure:
%
% General info:
%   .nTrain - number of training trials
%   .nTrial - number of test trial
%
% Trial info (.trial is replaced by .train in practice trials):
%   .trial(t).
%
% 2011 by Martin Rolfs

global visual scr keys const %#ok<NUSED>

% randomize random
rand('state',sum(100*clock));

design.levFiDu = 0.750;                         % minimum fixation duration [s]
design.levFiJi = 0.750;                         % additional fixation duration jitter [s]

if const.DEMO
    design.levFiDu = 1.500;                     % minimum fixation duration [s]
    design.levFiJi = 0.000;                     % additional fixation duration jitter [s]
    
    design.levTeDu = 0.500;                     % test stimulus duration [s]
    design.levTeOn = [0.01:0.05:0.21];          % test stimulus onset [s]
    
    design.levStDu = design.levTeDu;            % standard stimulus duration [s]
    design.levStOn =-1.000;                     % standard stimulus onset [s]
    
    design.levMaDu = 0.250;                     % test stimulus duration [s]
    
    design.levAfKe = 0.200;                     % recording time after keypress [s]
    design.levMaSa = 5.000;                     % maximum saccade latency [s]
else
    design.levTeDu = 0.030;                     % test stimulus duration [s]
    design.levTeOn = [0.01:0.05:0.21];          % test stimulus onset [s]
    
    design.levStDu = design.levTeDu;            % standard stimulus duration [s]
    design.levStOn =-0.500;                     % standard stimulus onset [s]
    
    design.levMaDu = 0.020;                     % test stimulus duration [s]
    
    design.levAfKe = 0.200;                     % recording time after keypress [s]
    design.levMaSa = 0.400;                     % maximum saccade latency [s]
end
design.levTePo = [1 2];                         % standard positions  [index for stiPosi]

design.sacRequ = 1;                             % 1 = saccade, 0 = cue but no saccade, -1 = neutral cue, no saccade

design.stiPosi = round([-10 0;10 0]*visual.ppd);% stimulus positions (relative to fixation)
design.stiFreq = [1 1.5];                       % stimulus spatial frequency [cpd]

% other variables
design.levFixX = 0.0;                           % eccentricity of fixation x (relative to screen center)
design.levFixY = 0.0;                           % eccentricity of fixation y (relative to screen center)

design.iti = 0.0;                               % inter-trial interval [s]

design.nBlocks = 1;                             % this number of blocks is shared by all sacRequ
design.nTrialsPerCellInBlock = 1;

design.levStCo = 10^-0.65;                      % standard stimulus contrast(s); for multiple: [10^-0.95 10^-0.65]
design.levCons = -1.85:0.15:-0.05;              % raster of contrasts potentially used
for st = 1:length(design.levStCo)
    % set up staircase procedures
    visual.ba(st).TiltPrior = SetPrior('mu',[10 10],'nu',[1 1],'range', [0.01 22.5]);
    visual.ba(st).TiltBayes = Initialise(visual.ba(st).TiltPrior,'T','p_threshold',.82);
    
    % set contrast range for standards
    % (!! Currently, using different ranges for each standard !!)
    [~,imin] = min(abs(log10(design.levStCo(st))-design.levCons));
    design.levTeCo(st,:) = 10.^design.levCons(imin-3:imin+3);
end

b = 0;
for sare = design.sacRequ
    for bl = 1:round(design.nBlocks/2)
        b = b + 1;
        t = 0;
        for itri = 1:design.nTrialsPerCellInBlock
            for tepo = 1:size(design.levTePo,2)
                for teco = 1:size(design.levTeCo,2)
                    for teon = 1:size(design.levTeOn,2)
                        for stco = 1:size(design.levStCo,2)
                            t = t + 1;
                            
                            fixx  = design.levFixX;
                            fixy  = design.levFixY;
                            
                            trial(t).sacReq = sare;   % 1 = saccade, 0 = cue but no saccade, -1 = neutral cue, no saccade
                            
                            trial(t).fixLoc = visual.scrCenter+round(visual.ppd*[fixx fixy fixx fixy]);
                            trial(t).fixCol = visual.white;
                            trial(t).marCol = visual.black;
                            
                            % temporal trial settings
                            trial(t).fixDur = round((design.levFiDu + design.levFiJi*rand)/scr.fd)*scr.fd;
                            trial(t).staTim = round(design.levStOn/scr.fd)*scr.fd;
                            trial(t).staDur = round(design.levStDu/scr.fd)*scr.fd;
                            trial(t).stmTim = trial(t).staTim + trial(t).staDur;
                            trial(t).stmDur = round(design.levMaDu/scr.fd)*scr.fd;
                            trial(t).tesTim = round(design.levTeOn(teon)/scr.fd)*scr.fd;
                            trial(t).tesDur = round(design.levTeDu/scr.fd)*scr.fd;
                            trial(t).temTim = trial(t).tesTim + trial(t).tesDur;
                            trial(t).temDur = round(design.levMaDu/scr.fd)*scr.fd;
                            
                            trial(t).maxSac = design.levMaSa;
                            trial(t).aftKey = design.levAfKe;
                            
                            randPha = 2*pi*rand;
                            randOri = sign(randn)*pi/4;
                            rf = randperm(length(design.stiFreq));
                            randFrq = design.stiFreq(rf(1))/visual.ppd;
                            for s = 1:size(design.stiPosi,1)
                                % spatial stimulus settings (standard)
                                trial(t).stan(s).col = visual.black;
                                trial(t).stan(s).loc = trial(t).fixLoc+[design.stiPosi(s,:) design.stiPosi(s,:)];
                                trial(t).stan(s).par = getGaborPars;
                                trial(t).stan(s).par.amp = design.levStCo(stco);
                                trial(t).stan(s).par.pha = randPha;
                                trial(t).stan(s).par.frq = randFrq;
                                trial(t).stan(s).par.ori = trial(t).stan(s).par.ori+randOri;
                            end
                            trial(t).test = trial(t).stan(tepo);
                            trial(t).test.par.amp = design.levTeCo(stco,teco);
                            %%% Note: ori will be determined in staircase !!
                            %%% trial(t).test.par.ori = design.stiTilt(round(rand)+1)+trial(t).stan(1).par.ori;
                            
                            % store stimulus features
                            trial(t).fixpox = fixx;
                            trial(t).fixpoy = fixy;
                            trial(t).tarpos = tepo; %#ok<*AGROW>
                            trial(t).tespox = design.stiPosi(tepo,1);
                            trial(t).tespoy = design.stiPosi(tepo,2);
                            trial(t).sacpos = tepo;
                            trial(t).tescon = trial(t).test.par.amp;
                            %%% trial(t).testil = trial(t).test.par.ori-trial(t).statil;% test tilt (relative to standard)
                            trial(t).stacon = trial(t).stan(1).par.amp;                 % standard contrast
                            trial(t).stcoin = stco;                                     % index of standard contrast
                            trial(t).statil = trial(t).stan(1).par.ori;                 % standard tilt (relative to zero)
                        end
                    end
                end
            end
            r = randperm(t);
            design.b(b).trial = trial(r);
        end
    end
end

design.blockOrder = 1:b;

design.nTrialsPB = t;   % number of trials per Block

save(sprintf('%s.mat',vpcode),'design','visual','scr','keys','const');
