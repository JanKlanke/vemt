function N = getBandpassNoisePatch(p)
%
% 2009 by Martin Rolfs

global visual

if length(p.ori)==1
    p.ori = [0 180];
end
if length(p.frq)==1
    p.frq = p.frq*[1/2 2];
end
if ~isfield(p,'cut')
    p.cut = 1;
end

[X,Y]    = meshgrid(-p.siz/2:p.siz/2,-p.siz/2:p.siz/2);
Noise    = randn(size(X));

ffilter = Bandpass2(size(X,1),p.frq(1)/visual.fNyquist,p.frq(2)/visual.fNyquist);
ofilter = OrientationBandpass(size(X,1),p.ori(1),p.ori(2));
cfilter = ofilter.*ffilter;

ft = cfilter.*fftshift(fft2(Noise));
Noise = real(ifft2(ifftshift(ft)));

Noise = Noise*(1/std(Noise(:)));
Noise = p.amp*Noise;

% limit contrast range of noise to be able 
% to add stimulus without truncation
Noise(Noise<-p.cut) = -p.cut;
Noise(Noise> p.cut) =  p.cut;


% only black and white
%Noise(Noise<0) = -1;
%Noise(Noise>=0) = 1;
Gaussian = exp(-(X.^2+Y.^2)./(2*p.sig^2)) ;

% Circle   = X.^2 + Y.^2 <= (p.siz/2)^2;      % circular boundry
N = Gaussian.*Noise;

