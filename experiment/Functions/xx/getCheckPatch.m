function [noi,p] = getCheckPatch(p)
%
% 2008 by Martin Rolfs

grain = round(2*p.siz*p.frq);
p.siz = grain * round(1/(2*p.frq));
noi = 2*(mosaic(2*p.siz,grain,0)-0.5);

