function Mask = getCircularMask(p)
%
% 2013 by Martin Rolfs

global visual

[X,Y] = meshgrid(-p.siz:p.siz,-p.siz:p.siz);

Mask = ones(2*p.siz+1, 2*p.siz+1, 2) * visual.bgColor;

Mask(:,:,2) = visual.white * (1 - ((X.^2 + Y.^2 <= p.siz^2)));
