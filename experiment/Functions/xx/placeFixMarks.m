function placeFixMarks(col,loc)
%
% 2008 by Martin Rolfs

global scr visual

pu = round(visual.ppd*0.1);

dis = round(2.5*visual.ppd);    % distance of markers
dia = 2*pu;                     % diameter of markers

%Screen(scr.main,'DrawLine',col,loc(1),loc(2)+dist,loc(1),loc(2)+dist+dia,4);
%Screen(scr.main,'DrawLine',col,loc(1),loc(2)-dist,loc(1),loc(2)-dist+dia,4);

Screen(scr.main,'FillOval',col,loc+[-dis-dia -dis-dia -dis -dis]);
Screen(scr.main,'FillOval',col,loc+[-dis-dia  dis -dis  dis+dia]);
Screen(scr.main,'FillOval',col,loc+[ dis -dis-dia  dis+dia -dis]);
Screen(scr.main,'FillOval',col,loc+[ dis  dis  dis+dia  dis+dia]);
