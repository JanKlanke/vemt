%Function to replace missed frames in random luminance sequence for the Vespa project. 

% Inputs: 
% - ols is the original requested luminance sequence 
% - mfis is a vector of indices of frames that occurred later than was requested. 
%This means that the previous frame ocurred twice; in other words the
%previous luminance value occurred on two sequential frames. 

% Outputs: 
% - nls is the fixed luminance sequence, with values prior to missed frames
% repeated once. 


function nls = replaceMissedFrames(ols,mfis) 

%ols should be column vector
if size(ols,1)<size(ols,2)
    ols = ols'; 
end
    
rfis = mfis-1; %indices of frames that need to be repeated 

%if mfi(1)=1, that means the first frame wasn't drawn until 1 refresh later
%than requested. Before that was 0. 
if rfis(1)==0
    nls = []; 
else
    nls = ols(1:rfis(1));
end

for fi = 1:length(rfis)
    strtf = rfis(fi);
    if fi<length(rfis)
        endf = rfis(fi+1);
    else
        endf = length(ols);
    end
    
    if strtf==0
        repbit=0;
    else 
        repbit = ols(strtf); 
    end
    
    if strtf<length(ols)
        nextbit = ols((strtf+1):endf);
    else
        nextbit = [];
    end
    
    %repeat the frame that lasted too lonag, and add the next part of the sequence up
    % until the next skipped frame, or until the end of the sequence,
    %if there is any more of the sequence.
    nls = [nls; repbit; nextbit];
   
end