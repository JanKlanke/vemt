function [data, design] = runSingleTrialPretest(td,design)
%
% td = trial design
%
% 2008 by Martin Rolfs (for Mac OSX)

global scr visual keys const


% clear keyboard buffer
FlushEvents('KeyDown');

% predefine boundary information
cxm = td.fixLoc(1);
cym = td.fixLoc(2);
sxm = cxm + td.tespox;
sym = cym + td.tespoy;

rad = visual.boundRad;
chk = visual.fixCkRad;

% get new values from staircase procedures
% td.test.par.amp = 10^NextStep(visual.ba(td.stcoin).ContBayes);
% td.tescon = td.test.par.amp;
dirTilt = sign(randn);
if const.DEMO
    td.testil = dirTilt*pi/8;
else
    td.testil = dirTilt*NextStep(visual.ba(td.stcoin).TiltBayes)/180*pi;
end
td.test.par.ori = td.statil+td.testil;
if const.TEST
    fprintf('\nContrast: %.3f, Tilt: %.1f',td.tescon,td.testil/pi*180);
end

% draw trial information on operator screen
Eyelink('command','draw_box %d %d %d %d 15', sxm-rad, sym-rad, sxm+rad, sym+rad);
Eyelink('command','draw_box %d %d %d %d 15', cxm-chk, cym-chk, cxm+chk, cym+chk);
for s = 1:length(td.stan)
    % gather standard stimulus information and generate texture
    sta(s).mat = visual.bgColor+visual.inColor*getGaborPatch(td.stan(s).par);
    sta(s).pos = CenterRectOnPoint([1 1 size(sta(s).mat)],td.stan(s).loc(1),td.stan(s).loc(2)); %#ok<*AGROW>
    sta(s).tex = Screen('MakeTexture', scr.main, sta(s).mat);
    
    % create masks for standard
    if s==1     % make sure to use the same mask for all standards
        stam(s).mat = visual.bgColor+visual.inColor*getBandpassNoisePatch(td.stan(s).par);
    end
    stam(s).pos = CenterRectOnPoint([1 1 size(stam(1).mat)],td.stan(s).loc(1),td.stan(s).loc(2)); %#ok<*AGROW>
    stam(s).tex = Screen('MakeTexture', scr.main, stam(1).mat);
    
    if s==td.sacpos
        Eyelink('command','draw_box %d %d %d %d 15', td.stan(td.sacpos).loc(1)-5, td.stan(td.sacpos).loc(2)-5, td.stan(td.sacpos).loc(1)+5, td.stan(td.sacpos).loc(2)+5);
        
        % gather test stimulus information and generate texture
        test.mat = visual.bgColor+visual.inColor*getGaborPatch(td.test.par);
        test.pos = CenterRectOnPoint([1 1 size(test.mat)],td.test.loc(1),td.test.loc(2)); %#ok<*AGROW>
        test.tex = Screen('MakeTexture', scr.main, test.mat);
        
        % create mask for test
        testm.mat = visual.bgColor+visual.inColor*getBandpassNoisePatch(td.test.par);
        testm.pos = CenterRectOnPoint([1 1 size(testm.mat)],td.test.loc(1),td.test.loc(2)); %#ok<*AGROW>
        testm.tex = Screen('MakeTexture', scr.main, testm.mat);
    end
    Eyelink('command','draw_filled_box %d %d %d %d 15', td.stan(s).loc(1)-10, td.stan(s).loc(2)-10, td.stan(s).loc(1)+10, td.stan(s).loc(2)+10);
end


% predefine time stamps
tFix    = NaN;  %#ok<*NASGU>
tStanOn = NaN;
tStaMOn = NaN;
tStanOf = NaN;
tGo     = NaN;
tTestOn = NaN;
tTesMOn = NaN;
tTestOf = NaN;
tSac    = NaN;
tRes    = NaN;
tClr    = NaN;

% Stimulus presentation: Fixation
for s=1:length(td.stan)
    placeFixMarks(td.stan(s).col,td.stan(s).loc);
end
drawFixation(td.fixCol,td.fixLoc);
Screen(scr.main,'DrawingFinished');
tFix = Screen(scr.main,'Flip');
Eyelink('message', 'EVENT_FixationDot');
if const.TEST; fprintf(1,'\nFixationDot'); end

% run pre-go-signal part of trial
eyePhase  = 1;  % 1 is fixation phase, 2 is saccade phase
breakIt   = 0;
fixBreak  = 0;
saccade   = 0;
stanNowOn = 0;
stanWasOn = 0;
stamNowOn = 0;
stamWasOn = 0;
testNowOn = 0;
testWasOn = 0;
tesmNowOn = 0;
tesmWasOn = 0;
goNowOn   = 0;
t = GetSecs;
while ~breakIt
    if ~stanNowOn && t >= (tFix + td.fixDur + td.staTim - scr.fd)
        % Stimulus presentation: Standard stimulus on
        for s=1:length(td.stan)
            Screen(scr.main,'DrawTexture',sta(s).tex,[],sta(s).pos);
            placeFixMarks(td.stan(s).col,td.stan(s).loc);
        end
        drawFixation(td.fixCol,td.fixLoc);
        Screen(scr.main,'DrawingFinished');
        tStanOn = Screen(scr.main,'Flip',tFix + td.fixDur + td.staTim - scr.fd/2);
        Eyelink('message', 'EVENT_StandardOn');
        if const.TEST; fprintf(1,'\nStandardOn'); end
        stanNowOn = 1;
    elseif ~stamNowOn && t >= (tFix + td.fixDur + td.stmTim - scr.fd)
        
        % Stimulus presentation: Standard mask on
        for s=1:length(td.stan)
            Screen(scr.main,'DrawTexture',stam(s).tex,[],stam(s).pos);
            placeFixMarks(td.stan(s).col,td.stan(s).loc);
        end
        drawFixation(td.fixCol,td.fixLoc);
        Screen(scr.main,'DrawingFinished');
        tStaMOn = Screen(scr.main,'Flip',tFix + td.fixDur + td.stmTim - scr.fd/2);
        Eyelink('message', 'EVENT_StandardMaskOn');
        if const.TEST; fprintf(1,'\nStandardMaskOn'); end
        stamNowOn = 1;
        stanWasOn = 1;
        
    elseif ~stamWasOn && t >= (tFix + td.fixDur + td.stmTim + td.stmDur - scr.fd)
        % Stimulus presentation: Standard stimulus off
        for s=1:length(td.stan)
            placeFixMarks(td.stan(s).col,td.stan(s).loc);
        end
        drawFixation(td.fixCol,td.fixLoc);
        Screen(scr.main,'DrawingFinished');
        tStanOf = Screen(scr.main,'Flip',tStanOn + td.staDur + td.stmDur - scr.fd/2);
        Eyelink('message', 'EVENT_StandardOff');
        if const.TEST; fprintf(1,'\nStandardOff'); end
        stamWasOn = 1;
    elseif ~goNowOn && t > (tFix + td.fixDur - scr.fd)
        % Stimulus presentation: Go signal
        for s=1:length(td.stan)
            placeFixMarks(td.stan(s).col,td.stan(s).loc);
        end
        if td.sacReq>=0
            drawGoSignal(td.stan(td.sacpos).col,td.fixLoc,td.stan(td.sacpos).loc);
        else
            for s = 1:length(td.stan)
                drawGoSignal(td.stan(s).col,td.fixLoc,td.stan(s).loc);
            end
        end
        drawFixation(td.fixCol,td.fixLoc);
        Screen(scr.main,'DrawingFinished');
        tGo = Screen(scr.main,'Flip',tFix + td.fixDur - scr.fd/2);
        Eyelink('message', 'EVENT_GoSignal');
        if const.TEST; fprintf(1,'\nGoSignal'); end
        if td.sacReq == 1
            eyePhase = 2;
        end
        goNowOn = 1;
    elseif ~testNowOn && t >= (tFix + td.fixDur + td.tesTim - scr.fd)
        % Stimulus presentation: Test stimulus on
        Screen(scr.main,'DrawTexture',test.tex,[],test.pos);
        for s=1:length(td.stan)
            placeFixMarks(td.stan(s).col,td.stan(s).loc);
        end
        if td.sacReq>=0
            drawGoSignal(td.stan(td.sacpos).col,td.fixLoc,td.stan(td.sacpos).loc);
        else
            for s = 1:length(td.stan)
                drawGoSignal(td.stan(s).col,td.fixLoc,td.stan(s).loc);
            end
        end
        drawFixation(td.fixCol,td.fixLoc);
        Screen(scr.main,'DrawingFinished');
        tTestOn  = Screen(scr.main,'Flip',tGo + td.tesTim - scr.fd/2);
        Eyelink('message', 'EVENT_TestOn');
        if const.TEST; fprintf(1,'\nTestOn'); end
        testNowOn = 1;
    elseif ~tesmNowOn && t >= (tFix + td.fixDur + td.temTim - scr.fd)
        
        % Stimulus presentation: Test mask on
        Screen(scr.main,'DrawTexture',testm.tex,[],testm.pos);
        for s=1:length(td.stan)
            placeFixMarks(td.stan(s).col,td.stan(s).loc);
        end
        if td.sacReq>=0
            drawGoSignal(td.stan(td.sacpos).col,td.fixLoc,td.stan(td.sacpos).loc);
        else
            for s = 1:length(td.stan)
                drawGoSignal(td.stan(s).col,td.fixLoc,td.stan(s).loc);
            end
        end
        drawFixation(td.fixCol,td.fixLoc);
        Screen(scr.main,'DrawingFinished');
        tTesMOn = Screen(scr.main,'Flip',tFix + td.fixDur + td.temTim - scr.fd/2);
        Eyelink('message', 'EVENT_TestMaskOn');
        if const.TEST; fprintf(1,'\nTestMaskOn'); end
        tesmNowOn = 1;
        testWasOn = 1;

    elseif ~tesmWasOn && t >= (tFix + td.fixDur + td.temTim + td.temDur - scr.fd)
        % Stimulus presentation: Test stimulus off
        for s=1:length(td.stan)
            placeFixMarks(td.stan(s).col,td.stan(s).loc);
        end
        if td.sacReq>=0
            drawGoSignal(td.stan(td.sacpos).col,td.fixLoc,td.stan(td.sacpos).loc);
        else
            for s = 1:length(td.stan)
                drawGoSignal(td.stan(s).col,td.fixLoc,td.stan(s).loc);
            end
        end
        drawFixation(td.fixCol,td.fixLoc);
        Screen(scr.main,'DrawingFinished');
        tTestOf  = Screen(scr.main,'Flip',tTestOn + td.tesDur + td.temDur - scr.fd/2);
        Eyelink('message', 'EVENT_TestOff');
        if const.TEST; fprintf(1,'\nTestOff'); end
        tesmWasOn = 1;
    end
    
    % eye position check
    [x,y] = getCoord;
    switch eyePhase
        case 1      % fixation phase
            if sqrt((x-cxm)^2+(y-cym)^2)>chk    % check fixation in a circular area
                fixBreak = 1;
            end
        case 2      % saccade phase
            if sqrt((x-cxm)^2+(y-cym)^2)>chk && isnan(tSac)
                tSac = GetSecs;
                if const.TEST; fprintf(1,'\nSaccade start'); end
            end
            if sqrt((x-sxm)^2+(y-sym)^2)<rad && ~saccade
                saccade=1;
                if const.TEST; fprintf(1,'\nEye at target'); end
            end
    end
    if fixBreak
        breakIt = 1;
    elseif  td.sacReq==1 && ~saccade && t > tFix + td.fixDur + td.maxSac
        breakIt = 2;
    elseif  td.sacReq==1 &&  saccade && tesmWasOn
        breakIt = 3;
    elseif  td.sacReq<=0 && tesmWasOn && goNowOn
        breakIt = 4;
    end
    
    t = GetSecs;
end

if const.TEST; fprintf(1,'\n\nbreakIt = %i\n\n',breakIt); end

switch breakIt
    case 1
        data = 'fixBreak';
        Eyelink('command','draw_text 100 100 15 Fixation break');
    case 2
        data = 'tooSlow';
        Eyelink('command','draw_text 100 100 15 Too slow');
    otherwise
        % check for keypress
        Snd('Play',[repmat(0.5,1,1050) linspace(0.5,0.0,50)].*[zeros(1,1000) sin(1:100)],5000);
        keyPress = 0;
        while ~keyPress % && ~const.TEST %%%%%%
            [keyPress, tRes] = checkTarPress(keys.respButtons);
        end
        % record a minimum of td.minKey sec after displacement
        % WaitSecs(tDis + td.minKey - GetSecs);
        WaitSecs(td.aftKey);
        
        rubber([]);
        Screen(scr.main,'DrawingFinished');
        tClr = Screen(scr.main,'Flip');
        Eyelink('message', 'EVENT_ClearScreen');
        if const.TEST; fprintf(1,'\nClearScreen'); end
        
        %-------------------------%
        % PREPARE DATA FOR OUTPUT %
        %-------------------------%
        % collect trial information
        trialData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%.4f\t%.4f\t%.4f\t%.4f',[td.fixpox td.fixpoy td.tespox td.tespoy round(1000*td.staTim) round(1000*td.tesTim) td.statil td.stacon td.testil td.tescon]);
        
        % determine presentation times relative to 1st frame
        timeData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i',round(1000*([tFix tStanOn tStanOf tGo tTestOn tTestOf tSac tRes tClr]-tGo)));
        % determine response data
        
        %if const.TEST %%%%%%
        %    tRes = tSac+0.5;
        %    keyPress = 1;
        %end
        
        if saccade
            sacRT = tSac - tGo;
        else
            sacRT = NaN;
            tSac  = NaN;
        end
        keyRT = tRes - tTestOn;
        
        resp = keys.respButtons(keyPress);
        switch resp
            case keys.keyLHiCcw
                respSide =-1;
                respCont = 1;
                respTilt =-1;
            case keys.keyLHiCw
                respSide =-1;
                respCont = 1;
                respTilt = 1;
            case keys.keyLLoCcw
                respSide =-1;
                respCont =-1;
                respTilt =-1;
            case keys.keyLLoCw
                respSide =-1;
                respCont =-1;
                respTilt = 1;
            case keys.keyRHiCcw
                respSide = 1;
                respCont = 1;
                respTilt =-1;
            case keys.keyRHiCw
                respSide = 1;
                respCont = 1;
                respTilt = 1;
            case keys.keyRLoCcw
                respSide = 1;
                respCont =-1;
                respTilt =-1;
            case keys.keyRLoCw
                respSide = 1;
                respCont =-1;
                respTilt = 1;
        end
        
        if respTilt == sign(td.testil)
            Snd('Play',[repmat(0.5,1,600) linspace(0.5,0.0,90)].*[zeros(1,600) sin(1:90)],7500);
        else
            Snd('Play',[repmat(0.5,1,200) linspace(0.5,0.0,30)].*[zeros(1,200) sin(1:30)],2500);
        end
        
        % update staircases only if saccade started after test presentation
        % or if no saccade was necessary
        if (td.sacReq==1 && tSac > tTestOf) || td.sacReq<=0
            % Update posterior for tilt staircase
            visual.ba(td.stcoin).TiltBayes = UpdatePosterior(abs(td.testil)/pi*180,respTilt==dirTilt,visual.ba(td.stcoin).TiltBayes);
            if const.TEST; fprintf(1,'\nStairCaseUpdated'); end
        end
        
        respData = sprintf('%i\t%i\t%i\t%i\t%i\t%i',round(1000*sacRT),round(1000*keyRT),respSide,respCont,respTilt,td.sacReq);
        
        % collect data for tab [9 x trialData 6%i3%.4f, 9 x timeData %i, 5 x respData %i]
        data = sprintf('%s\t%s\t%s',trialData, timeData, respData);
end
for s = 1:length(td.stan)
    Screen('Close',sta(s).tex);
end
Screen('Close',test.tex);

