function InitializePowermate()
% Initialize the Powermate knob

global knob keys
[a,b,c] = GetMouseIndices;
knob.a = a;

for i = 1:length(a)
    if (strcmp (b(i),'Griffin PowerMate') == 1) % needs the xserver evdev input driver
        powermateindex = i;
    end
end

knob.powermate = powermateindex;

[x,y,buttons,focus,valuator,valinfo] = GetMouse([],a(powermateindex));

knob.valuatorstart = valuator;
keys.respButtons = [1 2 3];     % 17 = n, 16 = m, 14 = k
keys.keyCCW = 1;                % for counterclockwise
keys.keyCW  = 2;                % for clockwise
keys.enter = 1000;
keys.resSpace  = KbName({'Space'});
% Checking the sampling rate of the powermate
knob.indextime = 0;
for i =1:1000
knob.start(i) = GetSecs;
end