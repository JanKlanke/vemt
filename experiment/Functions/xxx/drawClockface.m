function drawClockface(VecXY, colVec, windowptr)
% 2017 by Luke Pendergrass ( Version 'FrameArcs')
% adapted 2018 by Jan Klanke

Screen('DrawDots', windowptr, VecXY, 3, colVec, [], 1); % draws clockface by means of 3 pixel dots
end