function drawClockface_org(windowPtr, rect, startAngleVector, arcAngleVector, colorVector, sizeVector)
% org. by Luke Pendergrass 2017 ('FrameArcs') || adapted by Jan Klanke 2018

[rows, columns] = size(colorVector);

if rows > 1 
    for idx = 1:length(startAngleVector)
        Screen('FrameArc',windowPtr,[colorVector(1,idx) colorVector(2,idx) colorVector(3,idx)],rect,startAngleVector(idx),arcAngleVector(idx),sizeVector(idx));
    end
else
     for idx = 1:length(startAngleVector)
         Screen('FrameArc',windowPtr,colorVector(idx),rect,startAngleVector(idx),arcAngleVector(idx),sizeVector(idx));
     end
end

end