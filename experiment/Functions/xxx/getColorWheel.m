function pars = getColorWheel(fixaLoc, wheelDur, colWhlRad, colWhllinSiz)

global scr

colWhlRec = fixaLoc + round([-1 -1 1 1] * colWhlRad * visual.ppd);
colLinWid = dva2pix(colWhllinSiz, scr); 

% create color vector method 1)
brightGrad = 0:1/(wheelDur-1):1;           % create vector with values for brightness (lentgh := number of frames)
colVec = [2.0 * brightGrad; 2.0 * (1 - brightGrad); zeros(1,length(brightGrad))];   % transform values for brightness so that they become a color gradient in rgb spcace
colVec = colVec(randperm(3),:);            % randomly interchange color channels so as to become 6 different gradient-possibilities
colVec(colVec > 1) = 1;                    % 'cup' rgb values at 1 (not super important; would have been done either way)

% create color vector method 2 
% (in this method, the color gradient is corrected for different luminance
% levels)
% staCol = randsample([0 1/4 2/4 3/4],1);          % create color value to start from (first channel in hsv color space)
% finCol = staCol + 1/4;                           % create color value to finish with (first channel in hsv color space)
% trial(t).colVec = hsv2rgb([staCol:1/(wheelDur*4-1) :finCol;...  % create entire color vector based on start and final color and transform to rgb color space
%    ones(1,wheelDur);...
%    ones(1,wheelDur)]')';

% create report vector
colVecRep = [fliplr(colVec) colVec];
end