function [time, button, bufferINT, bufferOUT] = getPowerMate(bufferINT, bufferOUT, respOpt, device)
% 2018 by Jan Klanke 
% 2019 by Jan Klanke

global keys

[button, dialPos] = PsychPowerMate('Get', device);

if ~button  
    difference = bufferINT - dialPos;
    if checkTarPress(keys.pmFastMode)
        bufferOUT  = bufferOUT - 24 * difference;
    else
        bufferOUT  = bufferOUT - difference;
    end
    
    bufferOUT(bufferOUT < 0) = respOpt + bufferOUT(bufferOUT < 0); 
    bufferOUT(bufferOUT > respOpt) = bufferOUT(bufferOUT > respOpt) - respOpt;       
end

bufferINT = dialPos;
time = GetSecs;
end