function [data,eyeData] = runSingleTrial(td)
%
% td = trial design
%
% 2016 by Martin Rolfs

global scr visual keys const

% clear keyboard buffer
FlushEvents('KeyDown');

% Set the transparency for gabor patch
% Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);

% predefine boundary information
cxm = td.fixa.loc(1);
cym = td.fixa.loc(2);
sxm = td.fixa.loc(1);       %org.: sxm = td.targ.loc(1);
sym = td.fixa.loc(2);       %org.: sym = td.targ.loc(2);
rad = visual.boundRad;
chk = visual.fixCkRad;

% draw trial information on operator screen
if ~const.TEST
    Eyelink('command','draw_box %d %d %d %d 15', (sxm-rad)*2, (sym-rad)*2, (sxm+rad)*2, (sym+rad)*2);
    Eyelink('command','draw_box %d %d %d %d 15', (cxm-chk)*2, (cym-chk)*2, (cxm+chk)*2, (cym+chk)*2);
end

% generate Procedural Gabor textures
nStim = length(td.stims.pars.sizp);
sti.tex = visual.procGaborTex;
sti.vis = td.stims.pars.sizp;

% first generate vector for one rectangle, use this to generate the
% required number procedurally
% org.: sti.src = [zeros(2, nStim); sti.vis; sti.vis];
sti.src = [zeros(1,2) sti.vis(1) sti.vis(1)]; 
for ii= 1:nStim,
    sti.dst(:,ii) = CenterRectOnPoint(sti.src, td.stims.locX(ii), td.stims.locY(ii));
end
  
% precompute stimulus positions
idx1  = [1 3 5 7];
idx2  = [2 4 6 8];

for f = 1:td.totNFr
    for ff= 1:nStim,
        stiFrames(f).dst = sti.dst + repmat(td.posVec(idx1(ff):idx2(ff),f),2,nStim);
    end
end

% predefine time stamps
tFixaOn = NaN;  % t of fixation on
tStimOn = NaN;  % t of stimulus stream on
tMoveOn = NaN;  % t of movement cue on
tStimOf = NaN;  % t of stimulus off
tSac    = NaN;  % t of saccade (if any)
tRes    = NaN;  % t of response (if any)
tClr    = NaN;  % t of of clear screen

% set flags before starting stimulus stream
eyePhase  = 1;  % 1 is fixation phase, 2 is saccade phase
breakIt   = 0;
fixBreak  = 0;
saccade   = 0;

% eyeData
eyeData = [];

% Initialize vectors to store data on timing
frameTimes   = NaN(1,td.totNFr);
% flip screen to start out time counter for stimulus frames
firstFlip = 0;nextFlip = 0;
while ~firstFlip
   firstFlip = PsychProPixx('QueueImage', scr.myimg);
end
% set frame count to 0
f = 0;
% get a first timestap
tLoopBeg = GetSecs;
t = tLoopBeg;
while ~breakIt && f < td.totNFr
    f = f+1;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % stimulus presentation %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
    for slo = 1:const.sloMo
        Screen('FillRect', scr.myimg, visual.bgColor);
        % fixation
        if td.fixa.vis(f)
            drawFixation(td.fixa.col,td.fixa.loc);
%             if ~td.move.vis(f)
%                 drawGoSignal(td.move.col,td.fixa.loc);
%             end
        end
%         % saccade target
%         if td.targ.vis(f)
%             drawFixation(td.targ.col,td.targ.loc);
%         end
%         % movement cue
%         if td.move.vis(f)
%             drawGoSignal(td.move.col,td.targ.loc);
%         end
        % stimuli
        % Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);
        Screen('DrawTextures', scr.myimg, sti.tex, sti.src, stiFrames(f).dst, td.stims.pars.ori, [], [], [], [], kPsychDontDoRotation, [td.stims.pha(:,f), td.stims.pars.frqp', td.stims.pars.sigp', td.stims.amp(:,f), td.stims.pars.asp', zeros(nStim,3)]');

        % Flip
        nextFlip = PsychProPixx('QueueImage', scr.myimg);
    end
    %frameTimes(f) = GetSecs;
    
    %%%%%%%%%%%%%%%%%%%%%%%%
    % raise stimulus flags %
    %%%%%%%%%%%%%%%%%%%%%%%%

    % Send message that stimulus is now on
    if isnan(tFixaOn) && td.events(f)==1
        if ~const.TEST  ; Eyelink('message', 'EVENT_FixaOn'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_FixaOn'); end
        tFixaOn = frameTimes(f);
    end
%     if isnan(tMoveOn) && td.events(f)==2
%         if ~const.TEST  ; Eyelink('message', 'EVENT_MoveOn'); end
%         if  const.TEST>1; fprintf(1,'\nEVENT_MoveOn'); end
%         tMoveOn = frameTimes(f);
%         if td.sacReq == 1
%             eyePhase = 2;
%         end
%     end
    if isnan(tStimOn) && td.events(f)==2
        if ~const.TEST  ; Eyelink('message', 'EVENT_StimOn'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_StimOn'); end
        tStimOn = frameTimes(f);
    end
    if isnan(tStimOf) && td.events(f)==3
        if ~const.TEST  ; Eyelink('message', 'EVENT_StimOf'); end
        if  const.TEST>1; fprintf(1,'\nEVENT_StimOf'); end
        tStimOf = frameTimes(f);
    end
    
    % eye position check
    if const.TEST<2
        [x,y] = getCoord;
        switch eyePhase
            case 1      % fixation phase
                if sqrt((x-cxm)^2+(y-cym)^2)>chk    % check fixation in a circular area
                    fixBreak = 1;
                end
            case 2      % saccade phase
                % check if eyes are leaving fixation area
                if sqrt((x-cxm)^2+(y-cym)^2)>chk && isnan(tSac)
                    tSac = GetSecs;
                    if ~const.TEST, Eyelink('message', 'EVENT_Sac'); end
                end
                % check if eyes are reaching saccade target
                if sqrt((x-sxm)^2+(y-sym)^2)<rad && ~saccade
                    saccade=1;
                end
        end
    end
    if fixBreak
        breakIt = 1;    % fixation break
%     org.: elseif  td.sacReq==1 && ~saccade && t > tFixaOn + td.fixDur + td.maxSac
    elseif  ~saccade && t > tFixaOn + td.fixDur + td.maxSac
        if const.TEST<2
            breakIt = 2;    % no saccade in time
        end
%     org.: elseif  td.sacReq==1 &&  saccade &&  f == td.totNFr
    elseif  saccade &&  f == td.totNFr
        breakIt = 3;    % saccade in time, stop when stimulus is done
%     org.: elseif  td.sacReq<=0 &&  f == td.totNFr
    elseif  f == td.totNFr
        breakIt = 4;    % no saccade required, stop when stimulus is done
    end
    t = GetSecs;
end
lastFlip = 0;
while ~lastFlip && ~nextFlip
    Screen('FillRect', scr.myimg, visual.bgColor);
    lastFlip = PsychProPixx('QueueImage', scr.myimg);
end
tLoopEnd = GetSecs;

%%%%%%%%%%%%%%%%%%%%%%%%%
% stimulus presentation %
%%%%%%%%%%%%%%%%%%%%%%%%%
% Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
for i = 1:scr.rate
    Screen('FillRect', scr.myimg, visual.bgColor);
    
    % fixation
    if td.fixa.vis(f)
        drawFixation(td.fixa.col,td.fixa.loc);
%         if ~td.move.vis(f)
%             drawGoSignal(td.move.col,td.fixa.loc);
%         end
    end
%     % saccade target
%     if td.targ.vis(f)
%         drawFixation(td.targ.col,td.targ.loc);
%     end
%     % movement cue
%     if td.move.vis(f)
%         drawGoSignal(td.move.col,td.targ.loc);
%     end
    PsychProPixx('QueueImage', scr.myimg);%,frameTimes(f-1) + 0.5*scr.fd*scr.rate);
end
tStimOf = Screen('Flip',scr.main);%,frameTimes(f) + 0.5*scr.fd*scr.rate);
if const.TEST<2, Eyelink('message', 'EVENT_StimOf'); end
% switched of bc it flashed event StimOf twice
% if const.TEST; fprintf(1,'\nEVENT_StimOf'); end

switch breakIt
    case 1
        data = 'fixBreak';
        if const.TEST<2, Eyelink('command','draw_text 100 100 15 Fixation break'); end
    case 2
        data = 'tooSlow';
        if const.TEST<2, Eyelink('command','draw_text 100 100 15 Too slow'); end
    otherwise
        % check for keypress
        % Snd('Play',[repmat(0.5,1,1050) linspace(0.5,0.0,50)].*[zeros(1,1000) sin(1:100)],5000);
        keyPress = 0;
        while ~keyPress
            [keyPress, tRes] = checkTarPress(keys.respButtons);
        end
        WaitSecs(td.aftKey);
        
        for f = 1:scr.rate
            Screen('FillRect', scr.myimg, visual.bgColor);
            PsychProPixx('QueueImage', scr.myimg);
        end
        tClr = GetSecs;
        if const.TEST<2, Eyelink('message', 'EVENT_Clr'); end
        if const.TEST; fprintf(1,'\nEVENT_Clr'); end
        
        %-------------------------%
        % PREPARE DATA FOR OUTPUT %
        %-------------------------%
        % collect trial information
%       org.:  trialData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%.2f\t%i\t%i\t%.2f',[td.fixpox td.fixpoy td.tarpox td.tarpoy td.fixpos td.stiTil td.stiOff td.stiT2B td.velInc td.totNFr td.sacReq td.vpeakd]);
        trialData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%.2f\t%i\t%i\t%.2f',[td.fixpox td.fixpoy td.fixpos td.totNFr]);

        % if tMoveOn & tStimOn are simultaneous, only tStimOn will be available
        if isnan(tMoveOn)
            tMoveOn = tStimOn;
        end
        % determine presentation times relative to 1st frame
        timeData  = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i',round(1000*([tFixaOn tMoveOn tStimOn tStimOf tSac tRes tClr]-tMoveOn)));
        
        % determine response data
        if saccade
            sacRT = tSac - tMoveOn;
        else
            sacRT = NaN;
        end
        keyRT = tRes - tStimOf;
        
        % get response, defined as -1/1 for left/right tilt (like td.stiTil)
        if keys.respButtons(keyPress)==keys.resTopLeft
            resp = -1;
        elseif keys.respButtons(keyPress)==keys.resTopRight
            resp = 1;
        end
        respData = sprintf('%i\t%i\t%i',round(1000*sacRT),round(1000*keyRT),resp);
        
        % get information about how timing of frames worked
        tLoopFrames = round((tLoopEnd-tLoopBeg)/scr.fd);
        frameData = sprintf('%i\t%i',tLoopFrames,td.totNFr);

        % collect data for tab [12 x trialData %i, 7 x timeData %i, 3 x respData %i, 2 x frameData %i]
        data = sprintf('%s\t%s\t%s\t%s',trialData, timeData, respData, frameData);
end
