%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Variance-evaluation of paradigms to measure temporal events %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 2019 by Jan Klanke
%
% Observers have to fixate central fixation dot. After fixation period, a 
% Gabor grating will be presented that features a high velocity phase shift 
% (predicted peak velocity of a microsaccade) so as to be 
% invisible under normal viewing conditions. Only once the stimulus changes
% changes its onscreen location will the stimulus be visible. 
% Participants have to report the time at which they perceived the Gabor
% appear. Report is done either  
% (a) by adjusting the a clock hand that has been present during the
% stimulus presentation interval. 
% (b) by adjusting the clock hand to the supposed position of a clockhand
% that is only present prior to the stimulus presentation interval.
% (c) by adjusting the the length of a loading bar that has been presented
% during the stimulus presentation interval.
% (d) by adjusting the the length of a loading bar that has been presented
% prior to the stimulus presentation interval.
% (e) by presing the space bar as soon as they perceive the stimulus
% (reaction time condition).


clear all;
clear mex;
clear functions; 
delete(instrfindall);

addpath('Functions/','Data/');

home;
expStart=tic;

global const visual scr keys re %#ok<*NUSED>

const.TEST = 2;         % test in dummy mode? 0=with eyelink; 1=mouse as eye; 2=no gaze position checking of any sort
const.sloMo= 1;         % To play stimulus in slow motion, draw every frame const.sloMo times
re = RotaryEncoder('/dev/ttyS101');    % get rotary encoder 
    
exptname='VEMT';

try
    newFile = 0;
    while ~newFile
        [vpno, seno, subjectCode] = getVpCode(exptname);
        subjectCode = strcat('Data/',subjectCode);
        
        [blor] = getConditions;
        
        % create data file
        datFile = sprintf('%s.dat',subjectCode);
        if exist(datFile,'file')
            o = input('>>>> This file exists already. Should I overwrite it [y / n]? ','s');
            if strcmp(o,'y')
                newFile = 1;
            end
        else
            newFile = 1;
        end
  
    end
    
    % prepare screens
    prepScreen;
    
    % get key assignment
    getKeyAssignment;
    
    % disable keyboard
    ListenChar(2);
          
    % prepare stimuli
    prepStim;
    
    % generate design
    design = genDesign(vpno, seno, blor, subjectCode);
    
    % initialize eyelink-connection
    if const.TEST<2
        [el, err]=initEyelinkNew(subjectCode(6:end));
        if err==el.TERMINATE_KEY
            return
        end
    else
        el=[];
    end
    
    % runtrials
    design = runTrials(design,datFile,el);
    
    % shut down everything
    reddUp;
catch me
    rethrow(me);
    reddUp; %#ok<UNRCH>
end

expDur=toc(expStart);

fprintf(1,'\n\nThis (part of the) experiment took %.0f min.',(expDur)/60);
fprintf(1,'\n\nOK!\n');

plotReplayResults(datFile);